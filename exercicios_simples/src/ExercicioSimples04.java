import java.util.Scanner;

public class ExercicioSimples04 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double pot_lamp, larg_com, comp_com, area_com, pot_total;
		int num_lamp;
		
		System.out.println("Qual a potencia da lampada (em watts)? ");
		pot_lamp = sc.nextDouble();
		System.out.println("Qual a largura do comodo (em metros)? ");
		larg_com = sc.nextDouble();
		System.out.println("Qual o comprimento do comodo (em metros)?");
		comp_com = sc.nextDouble();
		
		area_com = larg_com * comp_com;
		pot_total = area_com * 18;
		num_lamp = (int)Math.round(pot_total/pot_lamp);
		System.out.println("N�mero de lampadas necessarias para iluminar esse comodo: " + num_lamp);
	}

}
