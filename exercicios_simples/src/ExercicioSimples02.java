import java.util.Scanner;

public class ExercicioSimples02 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double temp_f = 0, temp_c = 0;
		System.out.println("Informe a temperatura em graus Fahrenheit: ");
		temp_f = sc.nextDouble();
		temp_c = ((temp_f - 32) * 5) / 9;
		System.out.println("A temperatura em graus Celsius �: " + temp_c);
		
	}

}
